package com.company;

public class Coil {
    protected int numberOfBearings;
    protected int size;
    protected int weight;

    public int getNumberOfBearings(){return this.numberOfBearings;}
    public int getSize(){return this.size;}
    public int getWeight(){return this.weight;}


    public Coil(int numberOfBearings, int size, int weight)
    {
        this.numberOfBearings = numberOfBearings;
        this.size = size;
        this.weight = weight;
    }

    public Coil(Coil coil)
    {
        this.numberOfBearings = coil.numberOfBearings;
        this.size = coil.size;
        this.weight = coil.weight;
    }

    public String toString() {
        return "Coil--->" +
                " size:"
                + this.size
                + " weight:"
                + this.weight
                + " numberOfBearings:"
                + this.numberOfBearings;
    }
}
