package com.company;

public class SpinningCoil extends Coil{
    public SpinningCoil(int numberOfBearings, int size, int weight) {
        super(numberOfBearings, size, weight);
    }

    public SpinningCoil(Coil coil) {
        super(coil);
    }

    public String toString() {
        return "Spinning Coil--->" +
                " size:"
                + this.size
                + " weight:"
                + this.weight
                + " numberOfBearings:"
                + this.numberOfBearings;
    }
}
