package com.company;

public class FeederCoil extends Coil {
    public FeederCoil(int numberOfBearings, int size, int weight) {
        super(numberOfBearings, size, weight);
    }

    public FeederCoil(Coil coil) {
        super(coil);
    }

    public String toString() {
        return "Feeder Coil--->" +
                " size:"
                + this.size
                + " weight:"
                + this.weight
                + " numberOfBearings:"
                + this.numberOfBearings;
    }
}
