package com.company;

public class Feeder extends FishingRods{
    public FeederCoil coil;

    public Feeder(int length, int weight, int transportLength, FishingType type, Coil coil) {
        super(length, weight, transportLength, type, coil);
    }

    public Feeder(FishingRods r) {
        super(r);
    }

    public String toString() {
        return "Feeder--->" +
                " length:"
                + this.length
                + " weight:"
                + this.weight
                + " transportLength:"
                + this.transportLength
                + " type:"
                + this.type;
    }
}
