package com.company;

public enum FishingType {
    WINTER,
    DEFAULT,
    LAKE;

    private FishingType(){

    }
}
