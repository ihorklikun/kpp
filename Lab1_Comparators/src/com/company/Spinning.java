package com.company;

public class Spinning extends FishingRods{
    public SpinningCoil coil;


    public Spinning(int length, int weight, int transportLength, FishingType type, Coil coil) {
        super(length, weight, transportLength, type, coil);
    }

    public Spinning(FishingRods r) {
        super(r);
    }

    public String toString() {
        return "Spinning--->" +
                " length:"
                + this.length
                + " weight:"
                + this.weight
                + " transportLength:"
                + this.transportLength
                + " type:"
                + this.type;
    }
}
