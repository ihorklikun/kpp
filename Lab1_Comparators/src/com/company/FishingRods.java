package com.company;

public class FishingRods {
    protected int length;
    protected int weight;
    protected int transportLength;
    protected FishingType type;
    protected Coil coil;

    public FishingRods(int length, int weight, int transportLength, FishingType type, Coil coil)
    {
        this.length = length;
        this.weight = weight;
        this.transportLength = transportLength;
        this.type = type;
        this.coil = coil;
    }


    public FishingRods(FishingRods r)
    {
        this.length = r.length;
        this.weight = r.weight;
        this.transportLength = r.transportLength;
        this.type = r.type;
        this.coil = r.coil;
    }

    public String toString() {
        return "Rods--->" +
                " length:"
                + this.length
                + " weight:"
                + this.weight
                + " transportLength:"
                + this.transportLength
                + " type:"
                + this.type;
    }

    public String coilInfo(){
        return this.coil.toString();
    }

    public int getLength(){return this.length;}
    public int getWeight(){return this.weight;}
    public int getTransportLength(){return this.transportLength;}
    public FishingType getType(){return this.type;}


}
