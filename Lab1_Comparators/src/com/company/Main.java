package com.company;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        FishingStuffManager manager = new FishingStuffManager();

        ArrayList<FishingRods> rodsArrayList = new ArrayList<>();
        rodsArrayList.add(new Feeder(10,220,5,FishingType.WINTER,new FeederCoil(10,10,10)));
        rodsArrayList.add(new Feeder(11,220,6,FishingType.WINTER,new FeederCoil(10,10,10)));
        rodsArrayList.add(new Feeder(10,324,6,FishingType.LAKE,new FeederCoil(10,10,10)));
        rodsArrayList.add(new Feeder(13,156,5,FishingType.LAKE,new FeederCoil(10,10,10)));
        rodsArrayList.add(new Spinning(10,126,5,FishingType.LAKE,new SpinningCoil(10,10,10)));
        rodsArrayList.add(new Spinning(12,268,3,FishingType.LAKE,new SpinningCoil(10,10,10)));
        rodsArrayList.add(new Spinning(12,275,8,FishingType.DEFAULT,new SpinningCoil(10,10,10)));
        rodsArrayList.add(new Spinning(15,235,10,FishingType.DEFAULT,new SpinningCoil(10,10,10)));

        for (FishingRods rods: rodsArrayList)
        {
            System.out.println(rods.toString());
        }

        System.out.println("\n\nFIND BY WINTER TYPE");
        manager.setFishingRods(rodsArrayList);
        ShowRods(manager.findByType(FishingType.WINTER));

        System.out.println("\n\nSORT BY LENGTH");
        manager.sortByLength();
        ShowRods(manager.getFishingRods());

        System.out.println("\n\nSORT BY WEIGHT");
        manager.sortByWeight();
        ShowRods(manager.getFishingRods());

        System.out.println("\n\nSORT BY TRANSPORT LENGTH");
        manager.sortByTransportLength();
        ShowRods(manager.getFishingRods());

        System.out.println("\n\nSORT BY TRANSPORT LENGTH REVERSE");
        manager.sortByTransportLengthReverse();
        ShowRods(manager.getFishingRods());
    }

    public static void ShowRods(ArrayList<FishingRods> list)
    {
        for (FishingRods rods: list)
        {
            System.out.println(rods.toString());
        }
    }
}
