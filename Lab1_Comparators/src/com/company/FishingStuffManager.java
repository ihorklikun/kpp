package com.company;

import java.util.ArrayList;
import java.util.Comparator;

public class FishingStuffManager {
    private ArrayList<FishingRods> fishingRods;

    public FishingStuffManager()
    {
        this.fishingRods = new ArrayList<>();
    }
    public FishingStuffManager(ArrayList<FishingRods> fishingRods)
    {
        this.fishingRods = fishingRods;
    }

    public void setFishingRods(ArrayList<FishingRods> fishingRods){
        this.fishingRods = fishingRods;
    }
    public ArrayList<FishingRods> getFishingRods(){
        return this.fishingRods;
    }


    public ArrayList<FishingRods> findByType(FishingType type)
    {
        ArrayList<FishingRods> res = new ArrayList<>();
        for (FishingRods road: this.fishingRods)
        {
            if (road.type == type)
            {
                res.add(road);
            }
        }

        return res;
    }
    public void sortByLength()
    {
        this.fishingRods.sort(new FishingStuffManager.LengthComparator());
    }
    public void sortByWeight()
    {
        this.fishingRods.sort(new FishingStuffManager.WeightComparator());
    }
    public void sortByTransportLength()
    {
        this.fishingRods.sort(new Comparator<FishingRods>() {
            @Override
            public int compare(FishingRods o1, FishingRods o2) {
                return Integer.compare(o1.getTransportLength(),o2.getTransportLength());
            }
        });
    }
    public void sortByTransportLengthReverse()
    {
        this.fishingRods.sort((f1,f2)->{
            return (f1.getTransportLength() - f2.getTransportLength())* -1;
        });
    }

    static class LengthComparator implements Comparator<FishingRods>{
        public int compare(FishingRods f1, FishingRods f2){
            return Integer.compare(f1.getLength(),f2.getLength());
        }
    }
    class WeightComparator implements Comparator<FishingRods>{
        public int compare(FishingRods f1, FishingRods f2){
            return Integer.compare(f1.getWeight(),f2.getWeight());
        }
    }

}
