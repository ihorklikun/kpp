package com.company;

import java.util.HashMap;
import java.util.Map;

public enum Position {
    JUNIOR(0),
    MIDDLE(1),
    SENIOR(2);

    private  int value;
    private static Map map = new HashMap<>();

    Position(int value) {
        this.value = value;
    }

    static {
        for (Position positionType : Position.values()) {
            map.put(positionType.value, positionType);
        }
    }

    public static Position valueOf(int positionType) {
        return (Position) map.get(positionType);
    }

    public int getValue() {
        return value;
    }

    public static int getCount(){
        return map.size();
    }

}
