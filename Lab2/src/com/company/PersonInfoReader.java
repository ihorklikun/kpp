package com.company;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class PersonInfoReader {

    public static ArrayList<PersonInfo> readPersonsInfo(String path){
        ArrayList<PersonInfo> res = new ArrayList<>();

        String line;
        try {
            BufferedReader bufferReader = new BufferedReader(new FileReader(path));
            while ((line = bufferReader.readLine()) != null) {
                res.add(parsePersonInfo(line));
            }
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return res;
    }

    private static PersonInfo parsePersonInfo(String line) throws ParseException {
        PersonInfo personInfo = new PersonInfo();
        String array[] = line.split(" ");

        personInfo.lastName = array[0];
        personInfo.dateOfEnrollment = new SimpleDateFormat("dd/MM/yyyy").parse(array[1]);
        personInfo.positionType = Position.valueOf(array[2]);
        return personInfo;
    }
}
