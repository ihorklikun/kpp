package com.company;

import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

public class PersonInfo {
    public String lastName;
    public Date dateOfEnrollment;
    public Position positionType;

    public PersonInfo(String lastName, Date dateOfEnrollment, Position positionType) {
        this.lastName = lastName;
        this.dateOfEnrollment = dateOfEnrollment;
        this.positionType = positionType;
    }
    public PersonInfo() {
        this.lastName = "";
        this.dateOfEnrollment = null;
        this.positionType = null;
    }

    @Override
    public String toString() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        return "PersonInfo{" +
                "lastName='" + lastName + '\'' +
                ", dateOfEnrollment=" + formatter.format(dateOfEnrollment) +
                ", positionType=" + positionType +
                '}';
    }

    public static class PositionTypeComparator implements Comparator<PersonInfo> {
        public int compare(PersonInfo p1, PersonInfo p2){
            return Integer.compare(p1.positionType.getValue(),p2.positionType.getValue());
        }
    }

}
