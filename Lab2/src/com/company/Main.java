package com.company;

import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static java.time.temporal.ChronoUnit.DAYS;

public class Main {

    public static void main(String[] args) {



        ArrayList<PersonInfo> personInfos;
        HashMap<Date,ArrayList<PersonInfo>> hashMap = new HashMap<>();
        ArrayList<PersonInfo> list1 = new ArrayList<>();
        ArrayList<PersonInfo> list2 = new ArrayList<>();
        ArrayList<PersonInfo> multiplyResult = new ArrayList<>();

        Scanner in = new Scanner(System.in);

        while (true){
            System.out.println("1 - Зчитати з файлу та сформуваати HashMap");
            System.out.println("2 - Відсортувати HashMap");
            System.out.println("3 - Вилучити всіх осіб, для яких на біжучу дату зарахування відбулось більше 10 років назад");
            System.out.println("4 - Зчитати з 2 файлів");
            System.out.println("5 - Вивести дві колекції");
            System.out.println("6 - Сформувати колекцію в яких немає повторів прізвищ");
            System.out.println("7 - Вивести частотну таблицю посад з колекції");
            System.out.println("0 - Вихід");
            System.out.println("\nInput a number: ");
            int num = in.nextInt();

            switch (num) {
                case 1 -> {
                    personInfos = PersonInfoReader.readPersonsInfo("D:\\ЛП\\III курс\\КПП\\Lab2\\persons.txt");
                    hashMap = CreateMap(personInfos);
                    System.out.println("RESULT");
                    ShowHashMap(hashMap);
                    System.out.println("-------------------------------------");
                }
                case 2 -> {
                    sortHashMapPersons(hashMap);
                    System.out.println("RESULT");
                    ShowHashMap(hashMap);
                    System.out.println("-------------------------------------");
                }
                case 3 -> {
                    RemovePersons(hashMap);
                    System.out.println("RESULT");
                    ShowHashMap(hashMap);
                    System.out.println("-------------------------------------");
                }
                case 4 -> {
                    list1 = PersonInfoReader.readPersonsInfo("D:\\\\ЛП\\\\III курс\\\\КПП\\\\Lab2\\\\persons.txt");
                    list2 = PersonInfoReader.readPersonsInfo("D:\\\\ЛП\\\\III курс\\\\КПП\\\\Lab2\\\\persons2.txt");
                }
                case 5 -> {
                    System.out.println("RESULT");
                    System.out.println("\tLIST 1");
                    ShowPersons(list1);
                    System.out.println("\n\tLIST 2");
                    ShowPersons(list1);
                    System.out.println("-------------------------------------");
                }
                case 6 -> {
                    multiplyResult = MultiplyCollection(list1, list2);
                    System.out.println("RESULT");
                    ShowPersons(multiplyResult);
                    System.out.println("-------------------------------------");
                }
                case 7 -> {
                    GetPositionCounts(multiplyResult);
                }
            }
            if(num == 0){
                break;
            }
        }

    }

    public static void sortHashMapPersons(HashMap<Date,ArrayList<PersonInfo>> personInfos){
        for (Map.Entry<Date, ArrayList<PersonInfo>> entry : personInfos.entrySet()) {
            entry.getValue().sort(new PersonInfo.PositionTypeComparator());
        }
    }

    public static void ShowPersons(ArrayList<PersonInfo> personInfos){
        for (PersonInfo p: personInfos) {
            System.out.println("\t" + p.toString());
        }
    }

    public static HashMap<Date,ArrayList<PersonInfo>> CreateMap(ArrayList<PersonInfo> personInfos){
        HashMap<Date,ArrayList<PersonInfo>> hashMap = new HashMap<>();

        for (PersonInfo p : personInfos) {
            hashMap.put(p.dateOfEnrollment,new ArrayList<>());
        }
        for (PersonInfo p : personInfos) {
            hashMap.get(p.dateOfEnrollment).add(p);
        }

        return hashMap;
    }

    public static void ShowHashMap(HashMap<Date,ArrayList<PersonInfo>> personInfos){

        for (Map.Entry<Date, ArrayList<PersonInfo>> entry : personInfos.entrySet()) {
            System.out.println(entry.getKey());//+" : "+entry.getValue());
            ShowPersons(entry.getValue());
        }
    }

    public static void RemovePersons(HashMap<Date,ArrayList<PersonInfo>> personInfos) {
        ArrayList<Date> toRemove = new ArrayList<>();
        for (Map.Entry<Date, ArrayList<PersonInfo>> entry : personInfos.entrySet()) {
            var personDate = entry.getKey();

            DateTimeFormatter df = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

            LocalDate  d1 = LocalDate.parse(formatter.format(personDate), df);
            LocalDate  d2 = LocalDateTime.now().toLocalDate();

            Duration diff = Duration.between(d1.atStartOfDay(), d2.atStartOfDay());
            long diffYears = diff.toDays() / 365;
            if(diffYears > 10){
                toRemove.add(personDate);
            }
        }
        for(int i = 0;i< toRemove.size();i++){
            personInfos.remove(toRemove.get(i));
        }

    }

    public static ArrayList<PersonInfo> MultiplyCollection(ArrayList<PersonInfo> list1, ArrayList<PersonInfo> list2){
        ArrayList<PersonInfo> res = new ArrayList<>();
        AddUniquePersonToCollection(list1, res);
        AddUniquePersonToCollection(list2, res);
        return res;
    }

    private static void AddUniquePersonToCollection(ArrayList<PersonInfo> list1, ArrayList<PersonInfo> res) {
        for (PersonInfo list1Person: list1){
            boolean exist = false;
            for (PersonInfo resPerson: res){
                if(resPerson.lastName.equals(list1Person.lastName)){
                    exist = true;
                    break;
                }
            }
            if (!exist){
                res.add(list1Person);
            }
            exist = false;
        }
    }

    public static void GetPositionCounts(ArrayList<PersonInfo> list){
        int[] array = new int[Position.getCount()];
        for (int i = 0;i < Position.getCount();i++)
        {
            array[i] = 0;
        }

        for (int i = 0; i < Position.getCount();i++){
            for (PersonInfo p : list){
                if(p.positionType.equals(Position.valueOf(i))){
                    array[i]++;
                }
            }
        }

        for (int i = 0; i < Position.getCount();i++){
            System.out.println(Position.valueOf(i) + "  " + array[i]);
        }
    }

}
